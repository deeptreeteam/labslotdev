from django.conf.urls import include, url
from . import views

app_name = 'home'
# Moved urls to their respective apps
urlpatterns = [
    # /home/
    url(r'^$', views.IndexView.as_view(), name = 'index'),
    # Lab URLs needed for detail view
    url(r'^labs/', include('labs.urls')),
    # /registration and login
    # url(r'^userformview/$', views.UserFormView.as_view(), name = 'register'),
    # /about/
    url(r'^aboutview/$', views.AboutView.as_view(), name='about'),

]
