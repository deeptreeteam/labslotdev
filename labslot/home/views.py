"""
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
#from django.http import HttpResponse
from .models import Labs
from django.http import Http404


def index(request):
	all_labs = Labs.objects.all()
	return render(request, 'home/index.html', {'all_labs': all_labs})


def detail(request, labs_id):
	try:
		labs = Labs.objects.get(pk=labs_id)
	except Labs.DoesNotExist:
		raise Http404("Lab does not exist")
	return render(request, 'home/detail.html', {'labs': labs})
"""
from django.views import generic
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.views.generic import View
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from labs.models import Lab
from services.models import Services
from equipment.models import Equipment
# from .forms import UserForm
from django.conf import settings
import stripe # new
stripe.api_key = settings.STRIPE_SECRET_KEY # new

class IndexView(generic.ListView):
    context_object_name = 'labs'
    template_name = 'home/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['key'] = settings.STRIPE_PUBLISHABLE_KEY
        return context

    def get_queryset(self):
        return Lab.objects.filter(publish=True).order_by('-created_date')[:3]


class AboutView(generic.TemplateView):
    template_name = 'home/about.html'


# class UserFormView(View):
#     form_class = UserForm
#     template_name = 'home/registrationform.html'

#     # Display blank form
#     def get(self, request):
#         form = self.form_class(None)
#         return render(request, self.template_name, {'form': form})

#     # Process form data
#     def post(self, request):
#         form = self.form_class(request.POST)

#         if form.is_valid():
#             user = form.save(commit=False)

#             # Cleaned data
#             username = form.cleaned_data['username']
#             password = form.cleaned_data['password']
#             user.set_password(password)
#             user.save()

#             # Returns user object if credentials are correct
#             user = authenticate(username=username, password=password)

#             if user is not None:
#                 if user.is_active:
#                     login(request, user)
#                     return redirect('home:index')

#         return render(request, self.template_name, {'form': form})
