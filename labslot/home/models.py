# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django import forms
from django.core.urlresolvers import reverse

""" Added these  models to Labs app, do not delete until tested!
# Create your models here.
class Labs(models.Model):
    company_name = models.CharField(max_length=150)
    # company_logo = models.FileField()
    lab_type = models.CharField(max_length=150, default='Mechanical lab')
    company_address = models.CharField(max_length=500)
    # city = models.CharField(max_length = 200, default='City')
    country = models.CharField(max_length=200, default='Country')
    contact_person = models.CharField(max_length=80)
    contact_email = models.CharField(max_length=50)
    contact_phone = models.CharField(max_length=50)
    lab_price = models.CharField(max_length=100)
    time_period = models.CharField(max_length=10)
    # images = models.FileField()
    details = models.CharField(max_length=2500)

    def get_absolute_url(self):
        return reverse('home:detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.company_name + ' - ' + self.lab_type + ' - ' + self.company_address + ' - ' + "$" + self.lab_price + ' ' + self.time_period
"""

""" Added these  models to Services app, do not delete until tested!
class Services(models.Model):
    company_name = models.CharField(max_length=250, default='Company Name')
    service_category = models.CharField(max_length=250)
    service_type = models.CharField(max_length=250)
    contact_person = models.CharField(max_length=250)
    contact_email = models.CharField(max_length=250)
    contact_phone = models.CharField(max_length=250)
    service_location = models.CharField(max_length=250)
    service_details = models.CharField(max_length=2500)
    service_price = models.CharField(max_length=250, default=0)
    time_period = models.CharField(max_length=10, default='Per Hour')
    images = models.FileField()

    def get_absolute_url(self):
        return reverse('home:servicesview', kwargs={'pk': self.pk})
"""

""" Added these  models to Equipment app, do not delete until tested!
class Equipment(models.Model):
    company_name = models.CharField(max_length=250, default='Company Name')
    equipment_type = models.CharField(max_length=250)
    contact_person = models.CharField(max_length=250)
    contact_email = models.CharField(max_length=250)
    contact_phone = models.CharField(max_length=250)
    equipment_location = models.CharField(max_length=250)
    equipment_details = models.CharField(max_length=2500)
    equipment_price = models.CharField(max_length=250, default=0)
    time_period = models.CharField(max_length=10, default='Per Hour')
    images = models.FileField()

    def get_absolute_url(self):
        return reverse('home:equipmentview', kwargs={'pk': self.pk})
"""