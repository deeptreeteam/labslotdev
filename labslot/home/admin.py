# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
# from labs.models import Labs
from services.models import Services
from equipment.models import Equipment


# class LabsAdmin(admin.ModelAdmin):
#     list_display = ["company_name", "lab_type", "company_address", "city", "country", "contact_email", "lab_price",
#                     "time_period"]
#     list_filter = ["lab_type", "city", "country", "lab_price", "time_period"]
#     list_editable = ["lab_type", "contact_email"]
#     search_fields = ["company_name", "lab_type", "company_address", "city", "country", "contact_email", "lab_price",
#                      "details"]
#
#     class Meta:
#         model = Labs
#
#
# admin.site.register(Labs, LabsAdmin)
admin.site.register(Services)
admin.site.register(Equipment)
