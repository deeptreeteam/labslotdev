# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core.urlresolvers import reverse
from labs.models import Company
from django.contrib.auth import get_user_model
User = get_user_model()# Create your models here.
class ServiceCategory(models.Model):
    name = models.CharField(max_length=200)
    details = models.TextField()

    def __str__(self):
        return self.name



class Services(models.Model):
    company_name = models.ForeignKey(Company,on_delete = models.CASCADE,related_name = 'company_names',null=True,blank=True)
    service_category = models.ForeignKey(ServiceCategory,on_delete = models.CASCADE,related_name = 'service_category',null=True,blank=True)
    service_owner = models.ForeignKey(User,on_delete=models.CASCADE,related_name='service_owner',null=True,blank=True)
    service_name = models.CharField(max_length=250)
    contact_person = models.CharField(max_length=250)
    contact_email = models.EmailField(max_length=250)
    contact_phone = models.CharField(max_length=250)
    service_location = models.CharField(max_length=250)
    service_details = models.TextField(max_length=2500)
    service_price = models.CharField(max_length=250, default=0)
    time_period = models.TimeField()
    images = models.ImageField(upload_to='services/images/%Y/%m/%d', help_text=u'Upload Lab Image', blank=True,
                                     null=True)

    def __str__(self):
        return self.service_name

    def get_total_cost(self):
        return sum(item.get_cost() for item in self.service.all())



class Invoice(models.Model):
    service = models.ForeignKey(Services,on_delete = models.CASCADE,related_name = 'service')
    unit_price = models.DecimalField(max_digits=10, decimal_places=2)
    quantity = models.PositiveIntegerField(default=1)

    def __str__(self):
        return '{}'.format(self.id)

    def get_cost(self):
        print(self.unit_price * self.quantity)
        return self.unit_price * self.quantity

