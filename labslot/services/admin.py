# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Services,ServiceCategory,Invoice
# Register your models here.
admin.site.register(ServiceCategory)
admin.site.register(Invoice)