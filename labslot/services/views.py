# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.views import generic
from django.shortcuts import render, redirect
from django.views.generic import View
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from .models import Services


# class ServicesView(generic.ListView):
#     model = Services
#     template_name = 'home/equipment_listings.html'

#     def get_queryset(self):
#         return Services.objects.all()

    
# class DetailView(generic.DetailView):
#     model = Services
#     template_name = 'home/detail.html'


# class ServicesCreate(CreateView):
#     model = Services
#     fields = ['company_name', 'service_location', 'service_category', 'service_type', 'contact_person', 'contact_email',
#               'service_price', 'images', 'service_details']


# class ServicesUpdate(UpdateView):
#     model = Services
#     fields = ['company_name', 'service_location', 'service_category', 'service_type', 'contact_person', 'contact_email',
#               'service_price', 'images', 'service_details']


# class ServicesDelete(DeleteView):
#     model = Services
#     success_url = reverse_lazy('home:index')