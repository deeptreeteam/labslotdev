from django.conf.urls import url
from . import views

app_name = 'services'

urlpatterns = [
    # # /servicesview/
    # url(r'^servicesview/$', views.ServicesView.as_view(), name='services'),
    # # /home/"company_labs"/
    # url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    # # /home/services/add/
    # url(r'^services/add/$', views.ServicesCreate.as_view(), name='services-add'),
    # # Update Labs
    # url(r'^(?P<pk>[0-9]+)/$', views.ServicesUpdate.as_view(), name='services-update'),
    # # Delete Labs
    # url(r'^(?P<pk>[0-9]+)/delete/$', views.ServicesDelete.as_view(), name='services-delete'),
]
