from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.views.generic.base import TemplateView
from authentication import views
from labslot.decorators import anonymous_required

urlpatterns = [
    url(r'^logout/$', views.logout_view, name='logout'),
	url(r'^signup/$',anonymous_required(views.UserSignupForm.as_view()), name='signup'),
	url(r'^login/$', anonymous_required(auth_views.login), {'template_name': 'registration/login_form.html'}, name='login'),
]