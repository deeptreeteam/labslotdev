from django.shortcuts import redirect, render
from django.utils.decorators import method_decorator
from django.contrib.auth import logout
from authentication.models import User
from django.contrib.auth import authenticate, login
from django.views.generic import View
from .forms import SignUpForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AdminPasswordChangeForm, PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.contrib import messages
from django.shortcuts import render, redirect
from labs.models import Company


class UserSignupForm(View):
    form_class = SignUpForm
    template_name = 'registration/signup.html'

    # display blank form
    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'form': form})

    # process form data
    def post(self, request):
        form = self.form_class(request.POST)

        if form.is_valid():

            user = form.save(commit=False)

            # cleaned (normalized) data
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            company_name = form.cleaned_data['company_name']
            company_industry = form.cleaned_data['company_industry']
            company_address = form.cleaned_data['company_address']
            company_website = form.cleaned_data['company_website']
            company_product_type = form.cleaned_data['company_product_type']
            company_linkedin_profile = form.cleaned_data[
                'company_linked_in_profile']
           
            user.set_password(password)
            user.is_superuser = False
            user.save()

            

            user = authenticate(username=username, password=password)
            print(user)
            
            if user is not None:
                if user.is_active:
                    login(request, user)
                    company = Company.objects.create(owner = request.user,company_name=company_name,
                                             company_industry=company_industry, company_address=company_address, company_website=company_website,
                                             company_product_type=company_product_type, company_linked_in_profile=company_linkedin_profile)

            # returns these objects if credential are correct
                    company.save()
                    # after signup redirect to profile settings
                    return redirect('authentication:login')

        return render(request, self.template_name, {'form': form})


# logout
def logout_view(request):
    logout(request)
    return redirect('authentication:login')
