from django import forms 
from django.core.validators import MaxValueValidator, MinValueValidator
from .models import ReviewRating

class ReviewRatingForm(forms.ModelForm):
    review = forms.CharField(required = False, widget=forms.TextInput(attrs={'placeholder': 'Write Your Review', 'cols':50, 'rows':2}))
    rating = forms.IntegerField(required = True, validators=[MinValueValidator(1), MaxValueValidator(10)])

    class Meta:
        model = ReviewRating
        exclude = ('lab', 'user','created_date', 'updated_date')