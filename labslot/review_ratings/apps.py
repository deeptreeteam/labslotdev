from django.apps import AppConfig


class ReviewRatingsConfig(AppConfig):
    name = 'review_ratings'
