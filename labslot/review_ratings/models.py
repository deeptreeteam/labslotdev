from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.contrib.auth import get_user_model
User = get_user_model()
from labs.models import Lab
from django.contrib.contenttypes.fields import GenericRelation

# Create your models here
class ReviewRating(models.Model):
	lab = models.ForeignKey(Lab,on_delete=models.CASCADE,related_name='review_lab')
	user = models.ForeignKey(User,on_delete=models.CASCADE,related_name = 'review_user')
	review = models.TextField(null=True, blank=True)
	created_date = models.DateTimeField(auto_now_add=True)
	updated_date = models.DateTimeField(auto_now=True)
	rating = models.IntegerField(blank=False, default=1, validators=[MaxValueValidator(10), MinValueValidator(1)])
	def __str__(self):
		return self.review
