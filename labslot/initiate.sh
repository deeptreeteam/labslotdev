rm -rf authentication/migrations/
rm -rf labs/migrations/
rm -rf booking/migrations/
rm -rf review_ratings/migrations/
rm -rf services/migrations/
rm -f db.sqlite3

python manage.py makemigrations authentication
python manage.py migrate authentication

python manage.py makemigrations labs
python manage.py migrate labs

python manage.py makemigrations booking
python manage.py migrate booking

python manage.py makemigrations review_ratings
python manage.py migrate review_ratings

python manage.py makemigrations
python manage.py migrate
python manage.py migrate --run-syncdb

python manage.py createsuperuser