# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from labs.models import Lab
from django.contrib.auth import get_user_model

User = get_user_model()  # Create a Reservation Model which stores booking details


class Reservation(models.Model):
    STATUS = (
        ('Requested', "Requested"),
        ('Accepted', "Accepted"),
        ('Denied', "Denied"),
    )
    lab = models.ForeignKey(Lab, on_delete=models.CASCADE)
    username = models.CharField(max_length=255)
    CheckInDate = models.DateField()
    CheckOutDate = models.DateField()
    CheckInTime = models.TimeField()
    CheckOutTime = models.TimeField()
    message = models.TextField()
    status = models.CharField(choices=STATUS, default='Requested', max_length=50)

    class Meta:
        verbose_name_plural = 'Reservation'

    def __str__(self):
        return 'reservation.id {}-applicant.username '.format(self.id, self.username)
