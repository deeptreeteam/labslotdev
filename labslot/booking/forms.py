from django import forms
from .models import Reservation


class ReservationForm(forms.ModelForm):
    price = forms.CharField(max_length=20, widget=forms.TextInput(
        attrs={'readonly': 'readonly'}))
    username = forms.CharField(max_length=20, widget=forms.TextInput(
        attrs={'readonly': 'readonly'}))    

    class Meta:
        model = Reservation
        fields = ('lab', 'price','username','CheckInDate', 'CheckOutDate', 'CheckInTime', 'CheckOutTime', 'message')


class BookingStatusForm(forms.ModelForm):
	class Meta:
		model = Reservation
		fields = ('status',)