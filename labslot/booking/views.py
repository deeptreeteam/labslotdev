# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from datetime import datetime, timedelta
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse_lazy
import json
from django.shortcuts import render
from django.http import JsonResponse, HttpResponse, HttpResponseBadRequest
from django.template.loader import render_to_string
from django.views.generic import TemplateView, FormView, UpdateView
from django.views.generic.edit import CreateView
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Count, Sum
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from labs.models import Lab
from .models import Reservation
from .forms import ReservationForm, BookingStatusForm
from .models import Reservation
from django.views.generic import View
from django.shortcuts import get_object_or_404
from django.forms.models import model_to_dict
from authentication.models import User
from django.conf import settings
import stripe  # new

stripe.api_key = settings.STRIPE_SECRET_KEY  # new


# Create your views here.
class ReservationView(SuccessMessageMixin, FormView):
    template_name = 'lab_book_modal.html'
    initial = {}
    form_class = ReservationForm
    success_url = '/thanks/'
    success_message = 'Lab has been successfully booked.'
    data = dict()

    def get(self, request, *args, **kwargs):
        user_pk = request.user.pk
        user1 = User.objects.get(pk=user_pk)
        self.initial['lab'] = Lab.objects.get(pk=kwargs.get('pk'))
        self.initial['username'] = user1.username
        self.initial['type'] = Lab.objects.get(pk=kwargs.get('pk')).lab_type
        self.initial['price'] = Lab.objects.get(pk=kwargs.get('pk')).price
        print(self.initial['type'])
        form = self.form_class(initial=self.initial)
        self.data['html_form'] = render_to_string(self.template_name, {'form': form}, request=request)
        return JsonResponse(self.data)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            if request.user.is_authenticated:
                instance.user = request.user
            instance.save()
            self.data['form_is_valid'] = True
            return JsonResponse(self.data)
        else:
            self.data['form_is_valid'] = False
            self.data['html_form'] = render_to_string(self.template_name, {'form': form}, request=request)
        return JsonResponse(self.data)


def booking_list(request):
    booking_lab = Reservation.objects.filter(lab__company__owner=request.user).order_by('-id')
    key = settings.STRIPE_PUBLISHABLE_KEY
    return render(request, 'booking_list.html', {'booking_labs': booking_lab, 'key': key})


def my_booking_list(request):
    booking_lab = Reservation.objects.filter(username=request.user.username).order_by('-id')
    key = settings.STRIPE_PUBLISHABLE_KEY
    print(booking_lab)
    return render(request, 'my_booking_list.html', {'booking_labs': booking_lab, 'key': key})


"""
class MyBookingsView(ListView):
    def get(self,request ,pk):
        booking_lab = Reservation.objects.filter(username=request.user.username).order_by('-id')
        key = settings.STRIPE_PUBLISHABLE_KEY
        print(booking_lab)
        return render(request, 'my_booking_list.html', {'booking_labs': booking_lab, 'key': key})
"""


class BookingDetail(View):
    def get(self, request, pk):
        lab = get_object_or_404(Reservation, pk=pk)
        data = dict()
        data['lab'] = model_to_dict(lab)
        return JsonResponse(data)


class BookingStatusView(LoginRequiredMixin, UpdateView):
    model = Reservation
    form_class = BookingStatusForm
    template_name = 'booking_accept.html'
    success_url = reverse_lazy('booking:booking_lab_list')


class HomePageView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['key'] = settings.STRIPE_PUBLISHABLE_KEY
        return context


def charge(request, pk):
    print(pk)
    reservation = Reservation.objects.get(id=pk)
    lab = Lab.objects.get(id=reservation.lab.id)
    amount = lab.price
    print(lab)
    if request.method == 'POST':
        charge = stripe.Charge.create(
            amount=amount,
            currency='usd',
            description='A Django charge',
            source=request.POST['stripeToken']
        )
        return render(request, 'charge.html', {'amount': amount})

# from django.urls import reverse_lazy
# from django.contrib.messages.views import SuccessMessageMixin
# from django.views import generic
# from django.http import HttpResponseForbidden

# class ChargeView(SuccessMessageMixin,generic.DetailView):
#     success_url = '/success/'
#     success_message = "Payment Successfully Done"
#     template_name = 'my_booking_list.html'

#     def get_context_data(self, **kwargs):
#         context = super(ChargeView).get_context_data(**kwargs)
#         context['key'] = settings.STRIPE_PUBLISHABLE_KEY
#         return context

#     def post(self, request, *args, **kwargs):
#         lab = self.kwargs['pk']
#         amount = lab.amount
#         print(lab)
#         charge = stripe.Charge.create(
#             amount=amount,
#             currency='usd',
#             description='A Django charge',
#             source=request.POST['stripeToken']
#         )
#         return HttpResponseRedirect(self.request.path_info)

#     def get_success_url(self):
#         success_url = reverse_lazy('charge', {'id': self.object.pk})
#         return success_url
