from django.conf.urls import url
from django.views.generic import TemplateView
from django.contrib.auth import views as auth_views
from booking import views
urlpatterns=[
	url(r'^book/(?P<pk>\d+)$', views.ReservationView.as_view(), name='book'),
	url(r'^request/list/$', views.booking_list, name='booking_lab_list'),
	url(r'^mybooking/list/$', views.my_booking_list, name='my_booking_lab_list'),
	url(r'^details/(?P<pk>\d+)$', views.BookingDetail.as_view(), name='booking_detail'),
	url(r'^status/(?P<pk>\d+)$', views.BookingStatusView.as_view(), name='booking_accept'),
]
