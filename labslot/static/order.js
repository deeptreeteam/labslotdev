$(function() {

    /* Functions */

    var loadForm = function() {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function() {
                $("#modal-event").modal("show");
            },
            success: function(data) {
                $("#modal-event .modal-content").html(data.html_form);
            }
        });
    };

    var saveForm = function() {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function(data) {
                if (data.form_is_valid) {
                    $(".w3ls-header-right #test ul").html(data.html_user);
                    $("#modal-event").modal("hide");
                } else {
                    $("#modal-event .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };
    var orderForm = function() {
        var form = $(this);
        var abc = "suceessful"
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function(data) {
                if (data.form_is_valid) {

                    $(".w3ls-header-right #test ul").html(data.html_user);

                    $(".modal-body").html("<h2><center>Your order has been placed successfully. THANKYOU!!!</center></h2>");
                } else {
                    $("#modal-event .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };

    $(".js-add-order").click(loadForm);
    $("#modal-event").on("submit", ".js-order-pleced", orderForm);
});