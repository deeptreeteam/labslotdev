"""labslot URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from booking.views import charge

urlpatterns = [
    url(r'^charge/(?P<pk>\d+)/$', charge, name='charge'),
    url(r'^admin/', admin.site.urls),
    url(r'^', include('home.urls')),
    url(r'^authentication/', include('authentication.urls', namespace='authentication')),
    url(r'^userprofile/', include('userprofile.urls', namespace='userprofile')),
    url(r'^labs/', include('labs.urls', namespace='labs')),
    url(r'^booking/', include('booking.urls', namespace='booking')),
    url(r'^services/', include('services.urls')),
    url(r'^equipment/', include('equipment.urls')),
     url(r'^password_reset/$', auth_views.password_reset, {'template_name': 'registration/reset_my_password.html'}, name='password_reset'),
    url(r'^password_reset/done/$', auth_views.password_reset_done,{'template_name': 'registration/my_password_reset_done.html'}, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm,{'template_name': 'registration/my_password_reset_confirm.html'}, name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete,{'template_name': 'registration/my_password_reset_complete.html'}, name='password_reset_complete'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
