from django.conf.urls import url
from . import views

app_name = 'labs'

urlpatterns = [
    # /company/
    url(r'^company/create/$', views.CompanyCreateView.as_view(), name='company-add'),
    url(r'^company/update/(?P<pk>\d+)$', views.CompanyUpdateView.as_view(), name='company-update'),
    url(r'^company/delete/(?P<pk>\d+)$', views.CompanyDeleteView.as_view(), name='company-delete'),
    url(r'^company/detail/(?P<pk>\d+)$', views.CompanyDetailView.as_view(), name='company-detail'),
    url(r'^company/list/$', views.CompanyListView.as_view(), name='company-list'),

    # /labview/
    # url(r'^labview/$', views.LabView.as_view(), name='labview'),
    # # /lablistview/
    # url(r'^lablistview/$', views.LablistView.as_view, name='lablistview'),
    # # /detailview/
    # url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    # # /home/labs/add/
    # url(r'^labs/add/$', views.LabsCreate.as_view(), name='labs-add'),
    # # Update Labs
    # url(r'^(?P<pk>[0-9]+)/$', views.LabsUpdate.as_view(), name='labs-update'),
    # # Delete Labs
    # url(r'^(?P<pk>[0-9]+)/delete/$', views.LabsDelete.as_view(), name='labs-delete'),

    url(r'^create/', views.LabCreateView.as_view(), name='lab_add'),
    url(r'^list/$', views.LabListView.as_view(), name='lab_list'),
    url(r'^mylist/$', views.my_list, name='own_lab_list'),
    url(r'^details/(?P<pk>\d+)$', views.LabDetailView.as_view(), name='lab_detail'),
    url(r'^update/(?P<pk>\d+)$', views.LabUpdateView.as_view(), name='lab_update'),
    url(r'^delete/(?P<pk>\d+)$', views.LabDeleteView.as_view(), name='lab_delete'),

    url(r'^labfilter/$', views.MyList.as_view(), name='labfilter'),

    url(r'^(?P<lab_id>[0-9]+)/favorite/$', views.favorite, name='favorite'),
]
