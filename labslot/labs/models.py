# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()
from django.core.urlresolvers import reverse
from django.core.validators import MinValueValidator, MaxValueValidator
from django.contrib.contenttypes.fields import GenericRelation


class Company(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    company_name = models.CharField(max_length=150)
    company_address = models.CharField(max_length=250)
    company_website = models.URLField()
    company_industry = models.CharField(max_length=150)
    company_product_type = models.CharField(max_length=150)
    company_linked_in_profile = models.URLField()

    company_verified = models.BooleanField(default=False)

    def __str__(self):
        return self.company_name


class Lab(models.Model):
    LAB_TYPE = (
        ('science', 'Science'),
        ('engineering', 'Engineering'),
    )

    TIME_PERIOD = (
        ('hour', 'Hour'),
        ('day', 'Day'),
        ('week', 'Week'),
        ('month', 'Month'),
    )

    company = models.ForeignKey(Company, verbose_name="Company Name", on_delete=models.CASCADE)
    name = models.CharField(max_length=500, help_text='Lab Title')
    lab_type = models.CharField(choices=LAB_TYPE, max_length=150, default='science')
    street_address = models.CharField(max_length=300, null=True)
    city = models.CharField(max_length=200)
    country = models.CharField(max_length=200)
    details = models.TextField(help_text='Space, equipment and other facilities in the lab.', default='Stuff')
    contact_person = models.CharField(max_length=100, blank=True, null=True, help_text='Secondary Contact Person')
    contact_email = models.EmailField(max_length=100, blank=True, null=True, help_text='Secondary Contact Email')
    contact_phone = models.CharField(max_length=15)
    price = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(10000)],
                                verbose_name="Lab Price",
                                help_text='Lab Price (in NZ dollars)')
    time_period = models.CharField(choices=TIME_PERIOD, max_length=50)
    image1 = models.ImageField(upload_to='labs/images/%Y/%m/%d', help_text=u'Upload Lab Image', blank=False,
                               null=False)
    image2 = models.ImageField(upload_to='labs/images/%Y/%m/%d', help_text=u'Upload Lab Image', blank=False,
                               null=False)
    image3 = models.ImageField(upload_to='labs/images/%Y/%m/%d', help_text=u'Upload Lab Image', blank=False,
                               null=False)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    publish = models.BooleanField(default=True)
    average_ratings = models.FloatField(
        validators=[MinValueValidator(1), MaxValueValidator(5)], null=True, blank=True)
    lab_equipments = models.TextField(max_length=100, blank=True, null=True)
    lab_requirements = models.TextField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('labs:detail', kwargs={'pk': self.pk})


class Activity(models.Model):
    lab = models.ForeignKey(Lab, verbose_name="favourite lab",
                            on_delete=models.CASCADE, related_name='favourite_lab')
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='user_favourite')
    favorite = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username
