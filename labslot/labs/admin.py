from django.contrib import admin
from .models import (
    Company,
    Lab,
    Activity
)

admin.site.register(Company)
admin.site.register(Activity)



@admin.register(Lab)
class LabAdmin(admin.ModelAdmin):
    list_display = ('name', 'lab_type', 'city', 'contact_email', 'contact_phone', 'price', 'publish')
    list_editable = ('lab_type', 'contact_email', 'publish', )
    list_filter = ( 'country', 'city', 'lab_type', 'publish', 'created_date', 'price', 'time_period', )
    search_fields = ('name', 'city', 'country', 'lab_type')
    ordering = ('-updated_date',)


