from django import forms

from .models import Company, Lab


class CompanyForm(forms.ModelForm):
    class Meta:
        model = Company
        exclude = ('owner', 'company_verified')


class LabForm(forms.ModelForm):
    # company = forms.CharField(max_length=20, widget=forms.TextInput(
    #     attrs={'readonly': 'readonly'}))
    extra_field_count = forms.CharField(widget=forms.HiddenInput())

    class Meta:
        model = Lab
        exclude = ('created_date', 'updated_date',
                   'publish', 'average_ratings')
        fields = (
        'company', 'name', 'lab_type', 'street_address', 'city', 'country', 'contact_person', 'contact_email', 'contact_phone', 'price',
        'time_period', 'lab_equipments', 'lab_requirements', 'image1', 'image2', 'image3')

    def __init__(self, *args, **kwargs):
        lab_equipments = kwargs.pop('extra', 0)

        super(LabForm, self).__init__(*args, **kwargs)
        self.fields['extra_field_count'].initial = lab_equipments

        for index in range(int(lab_equipments)):
            self.fields['extra_field_{index}'.format(index=index)] = forms.CharField()
