from django.shortcuts import render, redirect, get_object_or_404
from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView, View
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Company, Lab
from .forms import CompanyForm, LabForm
from django.db.models import Q
from labs.models import Lab, Activity
from .filters import LabFilter
from django_filters.views import FilterView
from django_filters.views import BaseFilterView
from django.contrib.auth.decorators import login_required
from review_ratings.forms import ReviewRatingForm
from review_ratings.models import ReviewRating
from django.utils.decorators import method_decorator
from django.http import HttpResponseForbidden
from django.urls import reverse
from django.views.generic.edit import FormMixin
from django.http import HttpResponseRedirect
from review_ratings.models import ReviewRating
from django.contrib import messages
from labs.models import Company
from django.template.loader import render_to_string
from django.http import JsonResponse, HttpResponse, HttpResponseBadRequest
from django.shortcuts import render_to_response
from django.db.models import Avg


class CompanyListView(generic.ListView):
    model = Company
    context_object_name = 'companies'
    template_name = 'company/company_list.html'


class CompanyDetailView(generic.DetailView):
    model = Company
    context_object_name = 'company'
    template_name = 'company/company_detail.html'


class CompanyCreateView(LoginRequiredMixin, CreateView):
    model = Company
    form_class = CompanyForm
    template_name = 'company/company_create.html'
    success_url = reverse_lazy('labs:company-list')

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(CompanyCreateView, self).form_valid(form)


class CompanyUpdateView(LoginRequiredMixin, UpdateView):
    model = Company
    form_class = CompanyForm
    template_name = 'company/company_create.html'
    success_url = reverse_lazy('labs:company-list')


class CompanyDeleteView(LoginRequiredMixin, DeleteView):
    model = Company
    context_object_name = 'company'
    template_name = 'company/company_delete.html'
    success_url = reverse_lazy('labs:company-list')


# Lab Views starts from here....
def my_list(request):
    labs = Lab.objects.filter(company__owner=request.user)
    return render(request, 'labs/my_lab_list.html', {'labs': labs, })


class LabListView(generic.ListView):
    model = Lab
    context_object_name = 'labs'
    paginate_by = 10
    template_name = 'home/lab_listings.html'

    def get_queryset(self):
        return Lab.objects.filter(publish=True)


class LabDetailView(FormMixin, generic.DetailView):
    model = Lab
    context_object_name = 'lab'
    template_name = 'labs/detail.html'
    form_class = ReviewRatingForm

    def success_url(self):
        return reverse('labs:lab_detail', kwargs={'pk': self.object.pk})

    def get_context_data(self, **kwargs):
        lab = self.kwargs['pk']
        context = super(LabDetailView, self).get_context_data(**kwargs)
        obj = ReviewRating.objects.filter(lab=lab)
        avg = obj.aggregate(rating=Avg('rating'))
        average = avg['rating']
        lab = Lab.objects.get(id=lab)
        print(average)
        if average is None:
            average = 0
            lab.average_ratings = average
        else:
            average = round(average * 2) / 2
            lab.average_ratings = average
        lab.save()
        context.update({
            'reviews': ReviewRating.objects.filter(lab=lab)[:5],
            'average': average
        })
        return context

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        lab = self.kwargs['pk']
        form_post = Lab.objects.get(id=lab)
        if not request.user.is_authenticated:
            return HttpResponseForbidden
        self.object = self.get_object
        form = self.get_form()
        if form.is_valid():
            form.instance.user = self.request.user
            form.instance.lab = form_post

            form.save()
            return HttpResponseRedirect(self.request.path_info)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class LabCreateView(LoginRequiredMixin, CreateView):
    model = Lab
    form_class = LabForm
    template_name = 'labs/lab_create.html'
    success_url = reverse_lazy('labs:labfilter')

    def get(self, request, *args, **kwargs):
        user_pk = request.user.pk
        company = Company.objects.get(owner=request.user)
        self.initial['company'] = company.company_name
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        user = request.user
        company = Company.objects.get(owner=user)
        print("company", company)
        if company.company_verified:
            form = self.get_form()
            if form.is_valid():
                form_company = form.cleaned_data['company']
                print(form_company)
                if company == form_company:
                    form.save()
                    return self.form_valid(form)
                else:
                    messages.error(request, 'The Company You entered is not your company')
                    return render(request, self.template_name, {'form': form})


            else:
                return render(request, self.template_name, {'form': form})
        else:
            form = self.get_form()
            messages.error(request, 'Your Company has not been verified')
            return render(request, self.template_name, {'form': form})


class LabUpdateView(LoginRequiredMixin, UpdateView):
    model = Lab
    form_class = LabForm
    template_name = 'labs/lab_create.html'
    success_url = reverse_lazy('labs:lab_list')


class LabDeleteView(LoginRequiredMixin, DeleteView):
    model = Lab
    context_object_name = 'lab'
    template_name = 'labs/lab_delete.html'
    success_url = reverse_lazy('labs:lab_list')


class MyList(BaseFilterView, generic.ListView):
    model = Lab
    template_name = 'search/labs_filter.html'
    context_object_name = 'filters'
    filterset_class = LabFilter
    strict = False
    paginate_by = 4

    def get_queryset(self):
        queryset = Lab.objects.filter(publish=True).order_by('-id')
        return queryset


def favorite(request, lab_id):
    lab = get_object_or_404(Lab, pk=lab_id)
    print(lab)
    try:
        favorite = Activity.objects.create(
            lab=lab, user=request.user, favorite=True)
        favorite.save()
    except(KeyError, Lab.DoesNotExist):
        return render(request, 'labs/detail.html', {
            'lab': lab,
            'error_message': 'Invalid request'
        })

    return render(request, 'labs/detail.html', {'lab': lab, 'favorite': favorite})
