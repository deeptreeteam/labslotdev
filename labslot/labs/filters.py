from .models import Lab
import django_filters
from django.forms.widgets import TextInput

class LabFilter(django_filters.FilterSet):
	city = django_filters.CharFilter(lookup_expr='icontains' , widget=TextInput(attrs={'placeholder': 'Filter by city'}))
	name = django_filters.CharFilter(lookup_expr='icontains' , widget=TextInput(attrs={'placeholder': 'Search by lab name'}))
	country = django_filters.CharFilter(lookup_expr='icontains',widget=TextInput(attrs={'placeholder': 'Filter by country'}))
	price__lt = django_filters.NumberFilter(field_name='price',lookup_expr='lt')
	price__gt=django_filters.NumberFilter(field_name='price',lookup_expr='gt')
	class Meta:
		model = Lab
		fields = ['country','city','price','lab_type','name','time_period']





