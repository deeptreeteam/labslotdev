# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core.urlresolvers import reverse


class Equipment(models.Model):
    company_name = models.CharField(max_length=250, default='Company Name')
    equipment_type = models.CharField(max_length=250)
    contact_person = models.CharField(max_length=250)
    contact_email = models.EmailField(max_length=250)
    contact_phone = models.CharField(max_length=250)
    equipment_location = models.CharField(max_length=250)
    equipment_details = models.TextField(max_length=2500)
    equipment_price = models.CharField(max_length=250, default=0)
    time_period = models.CharField(max_length=10, default='Per Hour')
    images = models.ImageField()

    def get_absolute_url(self):
        return reverse('home:equipmentview', kwargs={'pk': self.pk})
