# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.views import generic
from django.shortcuts import render, redirect
from django.views.generic import View
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from .models import Equipment


class EquipmentView(generic.ListView):
    model = Equipment
    template_name = 'home/equipment_listings.html'

    def get_queryset(self):
        return Equipment.objects.all()


class DetailView(generic.DetailView):
    model = Equipment
    template_name = 'home/detail.html'


class EquipmentCreate(CreateView):
    model = Equipment
    fields = ['company_name', 'equipment_type', 'equipment_location', 'contact_person',
              'contact_email', 'equipment_details', 'equipment_price', 'time_period', 'details']


class EquipmentUpdate(UpdateView):
    model = Equipment
    fields = ['company_name', 'equipment_type', 'equipment_location', 'contact_person', 'contact_email',
              'equipment_details', 'equipment_price', 'time_period', 'details']


class EquipmentDelete(DeleteView):
    model = Equipment
    success_url = reverse_lazy('home:index')