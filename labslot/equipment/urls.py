from django.conf.urls import url
from . import views

app_name = 'equipment'

urlpatterns = [
    # /equipmentview/
    url(r'^equipmentview/$', views.EquipmentView.as_view(), name='equipment'),
    # /home/"company_labs"/
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    # /home/equipment/add/
    url(r'^equipment/add/$', views.EquipmentCreate.as_view(), name='equipment-add'),
    # Update Labs
    url(r'^(?P<pk>[0-9]+)/$', views.EquipmentUpdate.as_view(), name='equipment-update'),
    # Delete Labs
    url(r'^(?P<pk>[0-9]+)/delete/$', views.EquipmentDelete.as_view(), name='equipment-delete'),
]
